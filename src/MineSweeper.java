import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MineSweeper {
	private int rows;
	private int cols;
	private int numOfBombs;
	private boolean playing = true;
	private boolean won = false;
	private Buttons[][] field; // true = Bomb, false = no bomb
	JFrame game = new JFrame("MineSweeper");
	private boolean infoSet = false;

	MineSweeper() {

	}

	MineSweeper(int rows, int cols, int numOfBombs) {
		this.rows = rows;
		this.cols = cols;
		this.numOfBombs = numOfBombs;
	}

	public void setInfo(int rows, int cols, int numOfBombs) {
		this.rows = rows;
		this.cols = cols;
		this.numOfBombs = numOfBombs;
	}

	public void setUpGame() {
		JPanel p = new JPanel();
		field = new Buttons[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				field[i][j] = new Buttons();
				field[i][j].setSpot(false);
			}
		}
		Random r = new Random();
		for (int i = 0; i < numOfBombs; i++) {
			int x = r.nextInt(field.length);
			int y = r.nextInt(field[0].length);
			if (field[x][y].isSpot() == true) {
				i--;
			} else {
				field[x][y].setSpot(true);
			}
		}
		game.setSize((22 * cols + 20), (22 * rows + 20));
		game.setResizable(true);
		game.setLocationRelativeTo(null);
		game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p.setLayout(new GridLayout(rows, cols));
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				p.add(field[i][j]);
			}
		}
		game.add(p);
	}

	public void play() {
		game.setVisible(true);
		while (playing) {
			int tilesLeft = 0;
			for (int i = 0; i < field.length; i++) {
				for (int j = 0; j < field[i].length; j++) {
					if (field[i][j].isLeftClick()) {
						click(i, j);
						field[i][j].setLeftClick(false);
					}
					if (field[i][j].isRightClick()) {
						if (field[i][j].getIcon().equals(Buttons.flag)) {
							field[i][j].setIcon(Buttons.blank);
						} else {
							field[i][j].setIcon(Buttons.flag);
						}
						field[i][j].setRightClick(false);
					}
					if (field[i][j].getIcon().equals(Buttons.blank)) {
						tilesLeft++;
					}
				}
			}
			if (tilesLeft == 0) {
				playing = false;
				won = true;
			}
		}
		if (!won) {
			// System.out.println("lost");
			// game.setVisible(false);
			JFrame lost = new JFrame("Lost");
			lost.setSize(70, 70);
			lost.setResizable(true);
			lost.setLocationRelativeTo(null);
			lost.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			lost.add(new Label("You Lost."));
			lost.setVisible(true);
		}
		if (won) {
			JFrame won = new JFrame("Won");
			won.setSize(70, 70);
			won.setResizable(true);
			won.setLocationRelativeTo(null);
			won.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			won.add(new Label("You Won."));
			won.setVisible(true);
		}
	}

	private int getNeighbors(int rows, int columns) { // returns number of
		// neighbors
		// that have bombs
		int result = 0;
		// top
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// top right
		columns++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// mid right
		rows++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// lower right
		rows++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// bottom
		columns--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// bottom left
		columns--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// mid left
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// top left
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		return result;
	}

	public void click(int i, int j) {
		if (field[i][j].isAlive() == true) {
			field[i][j].setIcon(Buttons.bomb);
			playing = false; // end the game
			won = false;
		} else if (field[i][j].getIcon().equals(Buttons.white)) {
			return;
		} else {
			int surroundings = getNeighbors(i, j);
			switch (surroundings) {
			case 0:
				field[i][j].setIcon(Buttons.white);
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				break;
			case 1:
				field[i][j].setIcon(Buttons.one);
				break;
			case 2:
				field[i][j].setIcon(Buttons.two);
				break;
			case 3:
				field[i][j].setIcon(Buttons.three);
				break;
			case 4:
				field[i][j].setIcon(Buttons.four);
				break;
			case 5:
				field[i][j].setIcon(Buttons.five);
				break;
			case 6:
				field[i][j].setIcon(Buttons.six);
				break;
			case 7:
				field[i][j].setIcon(Buttons.seven);
				break;
			case 8:
				field[i][j].setIcon(Buttons.eight);
				break;
			}
		}
	}

	public void newGame() {
		JPanel p = new JPanel();
		JTextField r = new JTextField();
		JTextField c = new JTextField();
		JTextField b = new JTextField();
		p.setLayout(new GridLayout(3, 2));
		p.add(new JLabel("Rows:"));
		p.add(r);
		p.add(new JLabel("Columns:"));
		p.add(c);
		p.add(new JLabel("Bombs:"));
		p.add(b);
		JFrame menu = new JFrame("Welcome to MineSweeper");
		menu.setSize((500), (600));
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setLayout(new GridLayout(0, 1));
		menu.add(new JLabel("MineSweeper:"));
		menu.add(new JLabel("Left Click to select."));
		menu.add(new JLabel("Right Click to flag."));
		menu.add(new JLabel("Select difficulty:"));

		JButton easy = new JButton("Easy");
		easy.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					setInfo(9, 9, 10);
					infoSet = true;
				}
			}
		});
		JButton medium = new JButton("Medium");
		medium.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					setInfo(16, 16, 40);
					infoSet = true;
				}
			}
		});
		JButton hard = new JButton("Hard");
		hard.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					setInfo(16, 30, 99);
					infoSet = true;
				}
			}
		});
		JButton impossible = new JButton("Impossible");
		impossible.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					setInfo(20, 20, 399);
					infoSet = true;
				}
			}
		});
		JButton custom = new JButton("Custom");
		custom.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					// make custom game
					System.out.println("clicked");
					menu.setVisible(false);
					try {
						System.out.println("parsing");
						int rows = Integer.parseInt(r.getText());
						int cols = Integer.parseInt(c.getText());
						int bmbs = Integer.parseInt(b.getText());
						setInfo(rows, cols, bmbs);
						infoSet = true;
					} catch (NumberFormatException ee) {
						System.out.println("numberformatexction");
						menu.setVisible(true);
					}
				}
			}
		});
		menu.add(easy);
		menu.add(medium);
		menu.add(hard);
		menu.add(impossible);
		menu.add(p);
		menu.add(custom);
		menu.setVisible(true);

	}

	public static void main(String[] args) {
		MineSweeper m = new MineSweeper();
		m.newGame();
		while (!m.infoSet) {
			try {
				Thread.sleep(1000); // 1000 milliseconds is one second.
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
		m.setUpGame();
		m.play();
		// MineSweeper m = new MineSweeper(40,40,20);
		// m.setUpGame();
		// m.play();
	}
}