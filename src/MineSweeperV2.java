import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MineSweeperV2 extends JFrame {
	JPanel p = new JPanel();
	private boolean playing = true;
	private boolean won = false;
	private Buttons[][] field; // true = Bomb, false = no bomb

	MineSweeperV2(int rows, int columns, int numOfBombs) {
		super("MineSweeper");

		field = new Buttons[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				field[i][j] = new Buttons();
				field[i][j].setSpot(false);
			}
		}
		Random r = new Random();
		for (int i = 0; i < numOfBombs; i++) {
			int x = r.nextInt(field.length);
			int y = r.nextInt(field[0].length);
			if (field[x][y].isSpot() == true) {
				i--;
			} else {
				field[x][y].setSpot(true);
			}
		}
		setSize((22 * rows + 20), (22 * columns + 20));
		setResizable(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		p.setLayout(new GridLayout(rows, columns));
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				p.add(field[i][j]);
			}
		}
		add(p);
		while (playing) {
			int tilesLeft = 0;
			for (int i = 0; i < field.length; i++) {
				for (int j = 0; j < field[i].length; j++) {
					if (field[i][j].isLeftClick()) {
						click(i, j);
						field[i][j].setLeftClick(false);
					}
					if (field[i][j].isRightClick()) {
						field[i][j].setIcon(Buttons.flag);
						field[i][j].setRightClick(false);
					}
					if (field[i][j].getIcon().equals(Buttons.blank)) {
						tilesLeft++;
					}
				}
			}
			setVisible(true);
			if (tilesLeft == 0) {
				playing = false;
				won = true;
			}
			setVisible(true);
		}
		if (!won) {
			remove(p);
			add(new Button("You Lost."));
			setVisible(true);
		}
		if (won) {
			remove(p);
			add(new Button("You Won!!"));
			setVisible(true);
		}
	}

	private int getNeighbors(int rows, int columns) { // returns number of
														// neighbors
		// that have bombs
		int result = 0;
		// top
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// top right
		columns++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// mid right
		rows++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// lower right
		rows++;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// bottom
		columns--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// bottom left
		columns--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// mid left
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		// top left
		rows--;
		if (rows < field.length && columns < field[0].length && rows >= 0 && columns >= 0
				&& field[rows][columns].isSpot() == true) {
			result++;
		}
		return result;
	}

	public void click(int i, int j) {
		if (field[i][j].isSpot() == true) {
			field[i][j].setIcon(Buttons.bomb);
			playing = false; // end the game
		} else if (field[i][j].getIcon().equals(Buttons.zero)) {
			return;
		} else {
			int surroundings = getNeighbors(i, j);
			switch (surroundings) {
			case 0:
				field[i][j].setIcon(Buttons.zero);
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i++;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				j--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				i--;
				if (i < field.length && j < field[0].length && i >= 0 && j >= 0) {
					click(i, j);
				}
				break;
			case 1:
				field[i][j].setIcon(Buttons.one);
				break;
			case 2:
				field[i][j].setIcon(Buttons.two);
				break;
			case 3:
				field[i][j].setIcon(Buttons.three);
				break;
			case 4:
				field[i][j].setIcon(Buttons.four);
				break;
			case 5:
				field[i][j].setIcon(Buttons.five);
				break;
			case 6:
				field[i][j].setIcon(Buttons.six);
				break;
			case 7:
				field[i][j].setIcon(Buttons.seven);
				break;
			case 8:
				field[i][j].setIcon(Buttons.eight);
				break;
			}
		}
	}

	public static void playEasy() {
		new MineSweeperV2(6, 6, 10);
	}

	public static void playMedium() {
		new MineSweeperV2(10, 10, 35);
	}

	public static void playHard() {
		new MineSweeperV2(15, 15, 125);
	}

	public static void playImpossible() {
		new MineSweeperV2(20, 20, 399);
	}

	public static void play() {
		JFrame menu = new JFrame("Welcome to MineSweeper");
		menu.setSize((500), (300));
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
		menu.setDefaultCloseOperation(EXIT_ON_CLOSE);
		menu.setLayout(new GridLayout(0, 1));
		menu.add(new JLabel("MineSweeper:"));
		menu.add(new JLabel("Left Click to select."));
		menu.add(new JLabel("Right Click to flag."));
		menu.add(new JLabel("Select difficulty:"));

		JButton easy = new JButton("Easy");
		easy.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playEasy();
				}
			}
		});
		JButton medium = new JButton("Medium");
		medium.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playMedium();
				}
			}
		});
		JButton hard = new JButton("Hard");
		hard.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playHard();
				}
			}
		});
		JButton impossible = new JButton("Impossible");
		impossible.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playImpossible();
				}
			}
		});
		menu.add(easy);
		menu.add(medium);
		menu.add(hard);
		menu.add(impossible);
		menu.setVisible(true);
	}

	public static void main(String[] args) {
		MineSweeperV2.playEasy();
		// MineSweeperV2.play();
	}
}
