import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MineSweeperClient {
	public void play() {
		int diff = -1;
		JFrame menu = new JFrame("Welcome to MineSweeper");
		menu.setSize((500), (300));
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setLayout(new GridLayout(0, 1));
		menu.add(new JLabel("MineSweeper:"));
		menu.add(new JLabel("Left Click to select."));
		menu.add(new JLabel("Right Click to flag."));
		menu.add(new JLabel("Select difficulty:"));

		JButton easy = new JButton("Easy"){
			boolean isPlaying = false;
		};
		easy.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					isPlaying = true;
					MineSweeperV2.playEasy();
				}
			}
		});
		JButton medium = new JButton("Medium");
		medium.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playMedium();
				}
			}
		});
		JButton hard = new JButton("Hard");
		hard.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playHard();
				}
			}
		});
		JButton impossible = new JButton("Impossible");
		impossible.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					menu.setVisible(false);
					MineSweeperV2.playImpossible();
				}
			}
		});
		menu.add(easy);
		menu.add(medium);
		menu.add(hard);
		menu.add(impossible);
		menu.setVisible(true);
	}
	public static void main(String[] args){
		
	}
}
