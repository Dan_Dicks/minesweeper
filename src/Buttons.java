import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class Buttons extends JButton {
	private boolean alive; // true = bomb, false = no bomb
	private boolean rightClick = false;
	private boolean leftClick = false;

	public static String getCleanPath() {
		ClassLoader classLoader = Buttons.class.getClassLoader();
		File classpathRoot = new File(classLoader.getResource("").getPath());

		return classpathRoot.getPath();
	}

	public static ImageIcon zero = new ImageIcon(getCleanPath() + "/Zero.png");
	public static ImageIcon one = new ImageIcon(getCleanPath() + "/One.png");
	public static ImageIcon two = new ImageIcon(getCleanPath() + "/Two.png");
	public static ImageIcon three = new ImageIcon(getCleanPath() + "/Three.png");
	public static ImageIcon four = new ImageIcon(getCleanPath() + "/Four.png");
	public static ImageIcon five = new ImageIcon(getCleanPath() + "Five.png");
	public static ImageIcon six = new ImageIcon(getCleanPath() + "/Six.png");
	public static ImageIcon seven = new ImageIcon(getCleanPath() + "/Seven.png");
	public static ImageIcon eight = new ImageIcon(getCleanPath() + "/Eight.png");
	public static ImageIcon blank = new ImageIcon(getCleanPath() + "/Blank.png");
	public static ImageIcon bomb = new ImageIcon(getCleanPath() + "/Bomb.png");
	public static ImageIcon flag = new ImageIcon(getCleanPath() + "/Flag.png");
	public static ImageIcon white = new ImageIcon(getCleanPath() + "/white.png");


	public Buttons() {
		setIcon(blank);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					setRightClick(true);
				}
				if (e.getButton() == MouseEvent.BUTTON1) {
					setLeftClick(true);
				}
			}
		});
	}
	public boolean isAlive(){
		return alive;
	}

	public boolean isSpot() {
		return alive;
	}

	public void setSpot(boolean spot) {
		this.alive = spot;
	}

	public boolean isRightClick() {
		return rightClick;
	}

	public void setRightClick(boolean rightClick) {
		this.rightClick = rightClick;
	}

	public boolean isLeftClick() {
		return leftClick;
	}

	public void setLeftClick(boolean leftClick) {
		this.leftClick = leftClick;
	}

}
